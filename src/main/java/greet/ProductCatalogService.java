package greet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class ProductCatalogService {

    @Autowired
    private StockRepository stockRepo;

    @Autowired
    private ProductRepository prodRepo;

    @PostMapping("/product")
    public ResponseEntity<?> addProduct(@RequestBody Product product){
        prodRepo.save(product);
        return new ResponseEntity<Product>(product, HttpStatus.OK);
    }
    @GetMapping("/product/{productnumber}")
    public ResponseEntity<?> getProduct(@PathVariable("productnumber") Integer productnumber){
        Optional<Product> prod = prodRepo.findById(productnumber);
        return new ResponseEntity<Optional<Product>>(prod, HttpStatus.OK);
    }

    @PostMapping("/stock")
    public ResponseEntity<?> setStock(@RequestBody Stock stock){

        Optional<Product> prod = prodRepo.findById(stock.getProduct().getProductNumber());

        stock.setProduct(prod.orElse(null));
        stockRepo.save(stock);
        return new ResponseEntity<Stock>(stock, HttpStatus.OK);
    }

}
