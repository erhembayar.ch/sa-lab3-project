package greet;

import org.springframework.data.mongodb.core.aggregation.ArrayOperators;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CartLineRepository extends MongoRepository<CartLine, Long> {
}
