package greet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@RestController
public class ShoppingService {

    @Autowired
    private CartRepository cartRepo;

    @Autowired
    private CartLineRepository cartLineRepo;

    @Autowired
    private ProductRepository prodRepo;

    @PostMapping("/cart/{cartId}/{productnumber}/{quantity}")
    public ResponseEntity<?> addToCart(@PathVariable("cartId") int cartId, @PathVariable("productnumber") int productnumber, @PathVariable("quantity") long quantity){

        Optional<Product> prod =prodRepo.findById(productnumber);
        CartLine cartLine = new CartLine(new Random().nextLong(),quantity, prod.orElse(null));
        cartLineRepo.save(cartLine);
        Optional<ShoppingCart> cart = cartRepo.findById(cartId);
        ShoppingCart realCart = cart.orElse(new ShoppingCart(cartId));
        List<CartLine> cartLineList = realCart.getCartLines();
        cartLineList.add(cartLine);

        cartRepo.save(realCart);
        return new ResponseEntity<ShoppingCart>(cart.orElse(null), HttpStatus.OK);
    }


    @GetMapping("/cart/{cartId}")
    public ResponseEntity<?> getCart(@PathVariable("cartId") Integer cartId){
        Optional<ShoppingCart> cart = cartRepo.findById(cartId);
        return new ResponseEntity<ShoppingCart>(cart.orElse(null), HttpStatus.OK);

    }

}
