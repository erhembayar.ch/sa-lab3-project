package greet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public class BookController {

    @Autowired
    private BookRepository repository;

    @GetMapping("/book/{isbn}")
    public ResponseEntity<?> getBook(@PathVariable("isbn") String isbn){
        Optional<Book> book = repository.findById(isbn);
        return new ResponseEntity<Optional<Book>>(book, HttpStatus.OK);
    }

    @PostMapping("/book")
    public ResponseEntity<?> addBook(@RequestBody Book book){
        Book newBook = new Book(book.getIsbn(), book.getAuthor(), book.getTitle(), book.getPrice());
        repository.save(newBook);
        return new ResponseEntity<Book>(newBook, HttpStatus.OK);
    }

    @DeleteMapping("/book/{isbn}")
    public ResponseEntity<?> deleteBook(@PathVariable("isbn") String isbn){
        repository.deleteById(isbn);
        return new ResponseEntity<HttpStatus>(HttpStatus.OK);
    }

    @GetMapping("/books")
    public ResponseEntity<?> getAllBooks(){
        List<Book> list = repository.findAll();
        return new ResponseEntity<List<Book>>(list, HttpStatus.OK);
    }
}
