package greet;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;

@Getter
@Setter
@NoArgsConstructor
public class CartLine {

    public CartLine(long id, long quantity, Product product) {
        this.id = id;
        this.quantity = quantity;
        this.product = product;
    }

    @Id
    private long id;
    private long quantity;
    private Product product;

}
